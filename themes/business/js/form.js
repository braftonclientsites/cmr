jQuery(document).ready(function($){
	// Preload background images 
	var images = [];
	function preload() {
	    for (var i = 0; i < arguments.length; i++) {
	        images[i] = new Image();
	        images[i].src = preload.arguments[i];
	    }
	}
	preload(
		"/wp-content/themes/business/img/property.jpeg",
		"/wp-content/themes/business/img/group.jpeg",
		"/wp-content/themes/business/img/retirement.jpeg"
	)
	// Scroll to top of page when form icon is clicked
	$('.option').click(function() {
		$(window).scrollTop(0);
	});
	// Use query parameters to bring users who click on ctas to step 1 of the appropriate form
	if (window.location.search.indexOf('runOnStartup=1') > -1) {
	    $('#prop-form').show();
		$('.option.property .inner').hide();
		$('.text').hide();
		$('.option.group').hide();
		$('.option.retirement').hide();
		$('#prop-form').fadeIn();
		$('.background').addClass('property');
	} 
	if (window.location.search.indexOf('runOnStartup=2') > -1) {
	    $('#ret-form').show();
		$('.option.retirement .inner').hide();
		$('.text').hide();
		$('.option.property').hide();
		$('.option.group').hide();
		$('#ret-form').fadeIn();
		$('.background').addClass('retirement');
	} 
	if (window.location.search.indexOf('runOnStartup=3') > -1) {
	    $('#group-form').show();
		$('.option.group .inner').hide();
		$('.text').hide();
		$('.option.property').hide();
		$('.option.retirement').hide();
		$('#group-form').fadeIn();
		$('.background').addClass('group');
	} 
	// Define actions for each form icon
	$('.option.property .inner').click(function() {
		$('#prop-form').show();
		$(this).hide();
		$('.text').hide();
		$('.option.group').hide();
		$('.option.retirement').hide();
		$('#prop-form').fadeIn();
		$('.background').addClass('property');
	});
	$('.option.retirement .inner').click(function() {
		$('#ret-form').show();
		$(this).hide();
		$('.text').hide();
		$('.option.property').hide();
		$('.option.group').hide();
		$('#ret-form').fadeIn();
		$('.background').addClass('retirement');
	});
	$('.option.group .inner').click(function() {
		$('#group-form').show();
		$(this).hide();
		$('.text').hide();
		$('.option.property').hide();
		$('.option.retirement').hide();
		$('#group-form').fadeIn();
		$('.background').addClass('group');
	});
	// Reset everything when the user clicks the back button
	$('a.back').click(function() {
		$('.home-form').hide();
		$('.option .inner').fadeIn();
		$('.option').fadeIn();
		$('.text').fadeIn();
		$('.background').removeClass('property');
		$('.background').removeClass('retirement');
		$('.background').removeClass('group');
	});
	// Hide the last step of the form when user presses second back button
	$('a.drop-back').click(function() {
		$('.rest').hide();
		$('.dropdown').fadeIn();
	});
	// Display the last step of the form; hide dropdown
	$('a.next').click(function() {
		$('.dropdown').hide();
		$('.rest').fadeIn();
	});

	// Validate homepage forms using jquery.validate.min.js
	// Set defaults
	$.validator.setDefaults({
        ignore: []
    });
    $('#property-form').each(function() {
    	$(this).validate({
			rules: {
				message_naics: 		"required",
				message_other: 		{ 
					required: function(element) {
						return $('#message_naics option:selected').text() == "(81-81) Other Services (except Public Administration)";
					}
				},
				message_revenue: 	"required",
				message_country: 	"required",
				message_years: 		"required",
				message_email: 		"required",
				upload_file: 		"required"
			},
			messages: {
				message_naics: 		"Please select an NAICS/Industry type.",
				message_other: 		{required: "Please include your NAICS/Industry type."},
				message_revenue: 	"Please select an amount.",
				message_country: 	"Please select a country.",
				message_years: 		"Please select an option.",
				message_email: 		"Please enter a valid email address.",
				upload_file: 		"Please upload an attachment."
			}
	    });
    });
	
	$("#group-ben-form").each(function() {
		$(this).validate({
			rules: {
				message_naics: 		"required",
				message_other: 		{ 
					required: function(element) {
						return $('#message_naics2 option:selected').text() == "(81-81) Other Services (except Public Administration)";
					}
				},
				message_employees: 	"required",
				message_country: 	"required",
				message_years: 		"required",
				message_email: 		"required",
				upload_file: 		"required"
			},
			messages: {
				message_naics: 		"Please select an NAICS/Industry type.",
				message_other: 		{required: "Please include your NAICS/Industry type."},
				message_employees: 	"Please select an option.",
				message_country: 	"Please select a country.",
				message_years: 		"Please select an option.",
				message_email: 		"Please enter a valid email address.",
				upload_file: 		"Please upload an attachment."
			}
		});
	});

	$("#corp-ret-form").each(function() {
		$(this).validate({
			rules: {
				message_naics: 		"required",
				message_other: 		{ 
					required: function(element) {
						return $('#message_naics3 option:selected').text() == "(81-81) Other Services (except Public Administration)";
					}
				},
				message_employees: 	"required",
				message_assets: 	"required",
				message_country: 	"required",
				message_years: 		"required",
				message_email: 		"required",
			},
			messages: {
				message_naics: 		"Please select an NAICS/Industry type.",
				message_other: 		{required: "Please include your NAICS/Industry type."},
				message_employees: 	"Please select an option.",
				message_assets: 	"Please select an option.",
				message_country: 	"Please select a country.",
				message_years: 		"Please select an option.",
				message_email: 		"Please enter a valid email address."
			}
		});
	});
});