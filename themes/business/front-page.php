<?php

/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Yvonne's Theme 1.0
 */

$video = get_post_meta( get_queried_object_id(), 'bgvideo', true );
$color = get_post_meta( get_queried_object_id(), 'textcolor', true );
if ( !$video ) {
	$featured = has_post_thumbnail();
	$bg = get_post_meta( get_queried_object_id(), 'bgcolor', true );
	$shadow = get_post_meta( get_queried_object_id(), 'shadow', true );
}

  if (!empty($_POST['prop-submit']) || !empty($_POST['group-submit']) || !empty($_POST['ret-submit']) ) {

	  $mail = new PHPMailer();

	  //user posted variables
	  $naics = 			sanitizeInput( $_POST['message_naics'] ); //common
	  $naics_other = 	sanitizeInput( $_POST['message_other'] ); //common
	  $country = 		sanitizeInput( $_POST['message_country'] ); //common
	  $years = 			sanitizeInput( $_POST['message_years'] ); //common
	  $name = 			sanitizeInput( $_POST['message_name'] ); //common
	  $email = 			sanitizeInput( $_POST['message_email'] ); //common

	  switch($_POST){
		  case isset($_POST['prop-submit']):
		  		$heading = "Property/Casualty Form Submission";
				$revenue = 		sanitizeInput( $_POST['message_revenue'] ); 
				$returnUrl = get_site_url() . "/thank-you/";
		  break;
		  case isset($_POST['group-submit']):
		  		$heading = "Group Form Submission";
				$employees = 		sanitizeInput( $_POST['message_employees'] );
				$returnUrl = get_site_url() . "/thank-you-group/";
		  break;
		  case isset($_POST['ret-submit']):
		  		$heading = "Retirement Form Submission";
				$employees =	sanitizeInput( $_POST['message_employees'] );
	  			$assets = 		sanitizeInput( $_POST['message_assets'] );
				$returnUrl = get_site_url() . "/thank-you-retirement/";
		  break;
	  }


	  //Prop Form Fields

	  $message = "<h1>$heading</h1>";
	  $message .= "<ul>";
	  $message .= "<li><strong>NAICS Type:</strong> " . $naics . "</li>";
	  $message .= "<li><strong>Other NAICS Type?:</strong> " . $naics_other . "</li>";
	  if(isset($revenue)){ // Property/Casualty Form Submission
	  	$message .= "<li><strong>Revenue:</strong> " . $revenue . "</li>";
	  }elseif(isset($employees)){ // Group Form Submission & Retirement Form Submission
		$message .= "<li><strong># of Employees:</strong> " . $employees . "</li>";
	  }elseif(isset($assets)){ // Retirement Form Submission
		$message .= "<li><strong>Retirement Plan Assets:</strong> " . $assets . "</li>";
	  }
	  $message .= "<li><strong>Country:</strong> " . $country . "</li>";
	  $message .= "<li><strong>Years in Business:</strong> " . $years . "</li>";
	  $message .= "<li><strong>Name:</strong> " . $name . "</li>";
	  $message .= "<li><strong>Email:</strong> " . $email . "</li>";
	  $message .= "</ul>";

	  $mail->setFrom('chrismroche@hotmail.com', 'CMR Form Admin');
	  //$mail->addAddress('Cmrassoc@gmail.com', 'Christopher Roche');
	  $mail->addAddress('croche@cmr-associates.com', 'CMR Form Admin');
	  
	  		$username = "chrismroche@hotmail.com";
			$password = "6954XYZb!?";
			$host = "smtp.live.com";
			$port = 25;
			//$port = 587;
	  //Note this does not work on the brafton design server but smtp does work on cmr-dev site 
	  $mail->isSMTP();
	  $mail->Host = $host;
	  $mail->SMTPAuth = true;
	  
	  $mail->Username = $username;
	  $mail->Password = $password;
	  $mail->SMTPSecure = 'tls';
	  $mail->Port = $port;
	  $mail->addReplyTo('croche@cmr-associates.com', 'CMR Form Admin');
	  $mail->Subject  = $heading;
      $mail->Body 	  = $message;

      $mail->addAttachment( $_FILES['upload_file']['tmp_name'], $_FILES['upload_file']['name'] );

      $multi = $_FILES['multi_file_upload'];

      if (count($multi['tmp_name']) > 0) {
      	for ($i = 0; $i < count($multi['tmp_name']); $i++) {
      		$mail->addAttachment ($multi['tmp_name'][$i], $multi['name'][$i]);
      	}
      }

	  $mail->isHTML(true);


	   if(isset($_POST['g-recaptcha-response'])){
          $captcha=$_POST['g-recaptcha-response'];
        }
        if(!$captcha){
          echo '<h2>Please check the the captcha form.</h2>';
          exit;
        }
        $secretKey = "6LeUCRwUAAAAALlwRvsnTpbZdpbuMMLNH57ROZFl";
        $ip = $_SERVER['REMOTE_ADDR'];
        $response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secretKey."&response=".$captcha."&remoteip=".$ip);
        $responseKeys = json_decode($response,true);

        if(intval($responseKeys["success"]) !== 1) {
          echo '<h2>Your submission has been determined to be spam.</h2>';
        } else {
        	if(!$mail->send()) {
			  echo 'Message was not sent.';
			  echo 'Mailer error: ' . $mail->ErrorInfo;
			} else {
     	      header("Location: " . $returnUrl);
			}
        }
  }

get_header(); ?>
</header><!-- .site-header -->

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">

		<section><div style="<?php
			if ( $bg ) { echo 'background-color: ' . $bg . ';'; }
			if ( $color ) { echo 'color: ' . $color . ';'; }
			if ( $featured ) {
				echo 'background-image: url(';
				the_post_thumbnail_url();
				echo ');';
			} 
		?>" class="background<?php
			if ( $video ) { echo ' video'; }
		?>">

		<?php if ( $video ) {

			if (strpos($video, '.webm') == false && strpos($video, '.mp4') == false) {
				echo '<h5 class="warning">Use webm or mp4 files please.</h5>';
			} else {
				$vidstring = chop($video, '.webm');
				$vidstring = chop($vidstring, '.mp4'); ?>
				<button id="vidpause"><i class="fa fa-pause" aria-hidden="true"></i></button>
				<video playsinline autoplay muted loop id="bgvid">
					<?php if (strpos($video, '.webm') == true ) { ?>
						<source src="<?php echo $vidstring; ?>.webm" type="video/webm">
					<?php } else if (strpos($video, '.mp4') == true ) { ?>
						<source src="<?php echo $vidstring; ?>.mp4" type="video/mp4">
					<?php } ?>
				</video>
			<?php }

		} ?>

		<div class="overlay"><div class="site-inner"><?php
			while ( have_posts() ) : the_post();
		?>

			<div class="options">
				<div class="text">
					<h2><?php echo get_field('headline'); ?></h2>
				</div>

				<div class="option property">
					<div class="inner">
						<div class="thumb" style="background-image: url('<?php echo get_site_url(); ?>/wp-content/uploads/2017/02/Icons_PropertyCasualty2_PropertyCasualty.jpg');"></div>
						<h3>Global Property/Casualty</h3>
					</div>
				</div>
				<div class="option group">
					<div class="inner">
						<div class="thumb" style="background-image: url('<?php echo get_site_url(); ?>/wp-content/uploads/2017/02/Icons_Group-Benefits.jpg');"></div>
						<h3>Domestic & International<br />Group Benefits</h3>
					</div>
				</div>
				<div class="option retirement">
					<div class="inner">
						<div class="thumb" style="background-image: url('<?php echo get_site_url(); ?>/wp-content/uploads/2017/02/Icons_Retirement.jpg');"></div>
						<h3>Corporate Retirement</h3>
					</div>
				</div>
			</div>

			<?php include("inc/home-forms.php"); ?>

		</div></div></div><?php if ( $shadow ) { echo '<hr class="shadow"/>'; } ?></section>

		<div class="intro">
			<div class="site-inner">
				<?php the_content(); ?>
			</div>
		</div>

		<div class="stats">
			<div class="site-inner">
				<div class="row">
					<?php // stat fields
						$stat1 = get_field('stat_1_num');
						$stat1txt = get_field('stat_1_txt');

						$stat2 = get_field('stat_2_num');
						$stat2txt = get_field('stat_2_txt');

						$stat3 = get_field('stat_3_num');
						$stat3txt = get_field('stat_3_txt');

						$stat4 = get_field('stat_4_num');
						$stat4txt = get_field('stat_4_txt');
					?>
					<div class="col">
					 	<span class="large"><?php echo $stat1; ?></span><br />
						<?php echo $stat1txt; ?>
					</div>
					<div class="col">
						<span class="large"><?php echo $stat2; ?></span><br />
						<?php echo $stat2txt; ?>
					</div>
					<div class="col">
						<span class="large"><?php echo $stat3; ?></span><br />
						<?php echo $stat3txt; ?>
					</div> 
					<div class="col">
						<span class="large"><?php echo $stat4; ?></span><br />
						<?php echo $stat4txt; ?>
					</div>
				</div>
			</div>
		</div>


		<?php endwhile; ?>


		<?php // queue the services

			$service_array = array('post_type' => 'services', 'posts_per_page' => -1);
			$loop = new WP_QUERY ($service_array);

			if ($loop->have_posts() ) :

				echo '<div class="services"><div class="site-inner">';

			while ($loop->have_posts() ) : $loop->the_post(); 

			// call custom services fields
			$description = get_field('description');
			$sbtntxt = get_field('button_text');
			$sbtnurl = get_field('button_url');
			$sthumb = '';
				if (function_exists('has_post_thumbnail')) {
					if ( has_post_thumbnail() ) {
						$post_image_id = get_post_thumbnail_id($post_to_use->ID);
							if ($post_image_id) {
								$sthumb = wp_get_attachment_image_src( $post_image_id, 'large', false);
								if ($sthumb) (string)$sthumb = $sthumb[0];
							}
					}
				} 
			?>

			<div class="service">
				<a href="<?php the_permalink(); ?>">
					<div class="thumb" style="background-image: url('<?php echo $sthumb; ?>');"></div>
				</a>
				<h3><?php the_title(); ?></h3>
				<?php if ($description) { echo $description; } ?>
				<?php // button
					if ($sbtntxt) {
						echo "<div class='bottom'><a class='button' href=";
						echo $sbtnurl;
						echo ">";
						echo $sbtntxt;
						echo "</a></div>";
					} ?>
			</div>

			<?php

			endwhile;
			else :
			endif;
			echo '</div></div>'; 
			wp_reset_query(); ?>

		<?php // queue the testimonials 

			$test_array = array('post_type' => 'testimonials', 'posts_per_page' => -1);
			$loop2 = new WP_QUERY ($test_array);

			if ($loop2->have_posts() ) :

				echo '<div class="testimonials"><div class="site-inner"><ul class="testimonials-slider">';
			?>

			<script>
				$ = jQuery.noConflict(); 
				$(document).ready(function(){
				  $('.testimonials-slider').bxSlider({
				  	pager: false,
				  	auto: true,
				  	pause: 8000,
				  	speed: 1500,
				  	autoHover: true,
				  	nextSelector: '#slider-next',
				  	nextText: '<i class="fa fa-chevron-circle-right" aria-hidden="true"></i>',
				  	prevSelector: '#slider-prev',
				  	prevText: '<i class="fa fa-chevron-circle-left" aria-hidden="true"></i>',
			  		mode: 'horizontal',
			  		randomStart: true
				  });
				});
			</script>
		
		<?php

			while ($loop2->have_posts() ) : $loop2->the_post(); 

			// call custom services fields
			$quote = get_field('quote');
			$author = get_field('author');

			$tthumb = '';
				if (function_exists('has_post_thumbnail')) {
					if ( has_post_thumbnail() ) {
						$post_image_id = get_post_thumbnail_id($post_to_use->ID);
							if ($post_image_id) {
								$tthumb = wp_get_attachment_image_src( $post_image_id, 'large', false);
								if ($tthumb) (string)$tthumb = $tthumb[0];
							}
					}
				} 
			?>

			<li>
				<?php if ($quote) { ?>
					<span class="open-quote">
						<i class="fa fa-quote-left" aria-hidden="true"></i>
					</span>
					<span class="close-quote">
						<i class="fa fa-quote-right" aria-hidden="true"></i>
					</span>
					<div class="quote">
					<?php if ($tthumb) { ?>
						<div class="thumb" style="background-image: url('<?php echo $tthumb; ?>');"></div>
					<?php } ?>
					<?php echo $quote; ?>
					<div class="clear"></div>
					<?php if ($author) { ?>
						<span class="author">
							<em><?php echo $author; ?></em>
						</span>
					<?php } ?></div>
				<?php } ?>
			</li>

			<?php

			endwhile;
			else :
			endif;
			echo '</ul><div class="controls"><div id="slider-prev"></div><div id="slider-next"></div></div></div></div>'; 
			wp_reset_query(); ?>

	</main><!-- .site-main -->

</div><!-- .content-area -->
<?php if ( $video ) { ?>
	<script>
var vid = document.getElementById("bgvid"),
pauseButton = document.getElementById("vidpause");
function vidFade() {
    vid.classList.add("stopfade");
}
vid.addEventListener('ended', function() {
    // only functional if "loop" is removed 
     vid.pause();
	// to capture IE10
	vidFade();
});
pauseButton.addEventListener("click", function() {
    vid.classList.toggle("stopfade");
	if (vid.paused) {
vid.play();
		pauseButton.innerHTML = '<i class="fa fa-pause" aria-hidden="true"></i>';
	} else {
        vid.pause();
        pauseButton.innerHTML = '<i class="fa fa-play" aria-hidden="true"></i>';
	}
})
</script>
<?php }
get_footer(); ?>